
demo = {
    initPickColor: function () {
        $('.pick-class-label').click(function () {
            var new_class = $(this).attr('new-class');
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if (display_div.length) {
                var display_buttons = display_div.find('.btn');
                display_buttons.removeClass(old_class);
                display_buttons.addClass(new_class);
                display_div.attr('data-class', new_class);
            }
        });
    },

    // -------------------------------- Graph -------------------------------------------- //

    // ------ dateTime test ------- //

    // var date = new Date(-1485248629335*(-1));
    // document.getElementById("time").innerHTML = ("Format Date = " + date.toDateString()+ ", " + date.toLocaleTimeString());

    // --------- Users Local Data ----------- //
    initChartist: function () {

    }
};

//------------------------------------- FireBase -----------------------------------------------//

var app = angular.module("sampleApp", ["firebase"]);
app.controller("SampleCtrl", function ($scope, $firebaseArray, $filter) {

    var usersRef = firebase.database().ref().child('users').orderByChild('created_at').startAt(-999999999999999999999999).endAt(0);

    // download the data into a local object
    $scope.users = $firebaseArray(usersRef);

        // --------------------  Example Graph - Career --------------------- //

        var dayCountEmployee = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var dayCountOwner = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        usersRef.on("value", function(res){
            res.forEach(function(user){
                var dateString = user.child('created_at').val();
                var dayOfWeek = $filter('date')(new Date(dateString), 'EEE');
                if(user.child('currentApp/occupation').val() == 'owner'){
                    dayCountOwner[dayOfWeek] += 1;
                }else if(user.child('currentApp/occupation').val() == 'employee'){
                    dayCountEmployee[dayOfWeek] += 1;
                }

            });

            console.log("● จำนวนผู้สมัครกู้ในแต่ละวันในสัปดาห์ แยกตามประเภทอาชีพ ");

            console.log('พนักงานประจำ', dayCountEmployee);
            console.log('เจ้าของกิจการ', dayCountOwner);

            var regDayData = {
                labels: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                series: [
                    [dayCountEmployee.Sun, dayCountEmployee.Mon, dayCountEmployee.Tue, dayCountEmployee.Wed, dayCountEmployee.Thu, dayCountEmployee.Fri, dayCountEmployee.Sat],
                    [dayCountOwner.Sun, dayCountOwner.Mon, dayCountOwner.Tue, dayCountOwner.Wed, dayCountOwner.Thu, dayCountOwner.Fri, dayCountOwner.Sat]
                ]
            };

            var regDayOptions = {
                lineSmooth: true,
                low: 0,
                high: 50,
                stacked: true,
                height: "249px",

                axisX: {
                    showValues:"true",
                    showGrid: true
                }
            };

            var regDayResponsiveOptions = [
                ['screen and (max-width: 640px)', {

                    showValues:"true",
                    stacked: true,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ];

            Chartist.Line("#Example", regDayData, regDayOptions, regDayResponsiveOptions);
        });

        // -------------------- Example Graph - Operation System --------------------- //
        var countWin = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countIos = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countMac = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };
        var countAndroid = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        usersRef.on("value", function(res){
            res.forEach(function(user){
                var dateString = user.child('created_at').val();
                var dayOfWeek = $filter('date')(new Date(dateString), 'EEE');
                var osString = user.child('currentApp/profile/device/os').val();
                if(osString == 'windows'){
                    countWin[dayOfWeek] += 1;
                }else if(osString == 'mac'){
                    countMac[dayOfWeek] += 1;
                }else if(osString == 'ios'){
                    countIos[dayOfWeek] += 1;
                }else if(osString == 'android'){
                    countAndroid[dayOfWeek] += 1;
                }
            });


            console.log("● จำนวนผู้สมัครกู้ในแต่ละวันในสัปดาห์ แยกตาม OS ");

            console.log('windows', countWin);
            console.log('mac', countMac);
            console.log('ios', countIos);
            console.log('android', countAndroid);

            var regOsData = {
                labels: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                series: [
                    { "name": "Windows", "data": [countWin.Sun, countWin.Mon, countWin.Tue, countWin.Wed, countWin.Thu, countWin.Fri, countWin.Sat] },
                    { "name": "MacOS", "data": [countMac.Sun, countMac.Mon, countMac.Tue, countMac.Wed, countMac.Thu, countMac.Fri, countMac.Sat] },
                    { "name": "iOS", "data": [countIos.Sun, countIos.Mon, countIos.Tue, countIos.Wed, countIos.Thu, countIos.Fri, countIos.Sat] },
                    { "name": "Android", "data": [countAndroid.Sun, countAndroid.Mon, countAndroid.Tue, countAndroid.Wed, countAndroid.Thu, countAndroid.Fri, countAndroid.Sat] }
                ]
            };

            var regOsOptions = {
                lineSmooth: true,
                low: 0,
                high: 40,
                height: "249px",
                axisX: {
                    showGrid: true
                }
            };

            var regOsResponsiveOptions = [
                ['screen and (max-width: 640px)', {
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ];

            Chartist.Bar("#Example2", regOsData, regOsOptions, regOsResponsiveOptions);
        });

        // -------------------- Example Graph - Browser --------------------- //
        var countChrome = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countSafari = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countIE = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };
        var countUnknown = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        usersRef.on("value", function(res){
            res.forEach(function(user){
                var dateString = user.child('created_at').val();
                var dayOfWeek = $filter('date')(new Date(dateString), 'EEE');
                var browserString = user.child('currentApp/profile/device/browser').val();
                if(browserString == 'chrome'){
                    countChrome[dayOfWeek] += 1;
                }else if(browserString == 'safari'){
                    countSafari[dayOfWeek] += 1;
                }else if(browserString == 'ie'){
                    countIE[dayOfWeek] += 1;
                }else if(browserString == 'unknown'){
                    countUnknown[dayOfWeek] += 1;
                }
            });

            console.log("● จำนวนผู้สมัครกู้ในแต่ละวันในสัปดาห์ แยกตาม Browser ");

            console.log('chrome', countChrome);
            console.log('safari', countSafari);
            console.log('ie', countIE);
            console.log('unknown', countUnknown);

            var BrowserData = {
                labels: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                series: [
                    { "name": "Chrome", "data": [countChrome.Sun, countChrome.Mon, countChrome.Tue, countChrome.Wed, countChrome.Thu, countChrome.Fri, countChrome.Sat] },
                    { "name": "Safari", "data": [countSafari.Sun, countSafari.Mon, countSafari.Tue, countSafari.Wed, countSafari.Thu, countSafari.Fri, countSafari.Sat] },
                    { "name": "IE", "data": [countIE.Sun, countIE.Mon, countIE.Tue, countIE.Wed, countIE.Thu, countIE.Fri, countIE.Sat] },
                    { "name": "Unknown", "data": [countUnknown.Sun, countUnknown.Mon, countUnknown.Tue, countUnknown.Wed, countUnknown.Thu, countUnknown.Fri, countUnknown.Sat] }
                ]
            };

            var BrowserOptions = {
                lineSmooth: true,
                low: 0,
                high: 50,
                height: "249px",
                area: true,
                axisX: {
                    showGrid: true
                }
            };

            var BrowserResponsiveOptions = [
                ['screen and (max-width: 640px)', {
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ];

            Chartist.Line("#Example3", BrowserData, BrowserOptions, BrowserResponsiveOptions);
        });

        // -------------------- Example Graph - Location --------------------- //
        var countNorthern = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countCentral = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countNortheastern = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countEastern = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countSouthern = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        usersRef.on("value", function(res){
            res.forEach(function(user){
                var dateString = user.child('created_at').val();
                var dayOfWeek = $filter('date')(new Date(dateString), 'EEE');
                var localString = user.child('currentApp/profile/workPlace').val();
                if(localString == 'เชียงใหม่'||
                    localString =='เชียงราย'||
                    localString =='น่าน'||
                    localString =='พะเยา'||
                    localString =='แพร่'||
                    localString =='แม่ฮ่องสอน'||
                    localString =='ลำพูน'||
                    localString =='ลำปาง'||
                    localString =='ตาก'||
                    localString =='นครสวรรค์'||
                    localString =='พิจิตร'||
                    localString =='พิษณุโลก'||
                    localString =='อุตรดิตถ์'||
                    localString =='อุทัยธานี'||
                    localString =='เพชรบูรณ์'||
                    localString =='สุโขทัย'||
                    localString =='กำแพงเพชร'){
                    countNorthern[dayOfWeek] += 1;

                }else if(localString == 'กทม'||
                    localString =='กรุงเทพฯ'||
                    localString =='กรุงเทพ'||
                    localString =='ชัยนาท'||
                    localString =='นครนายก'||
                    localString =='นครปฐม'||
                    localString =='นนทบุรี'||
                    localString =='ปทุมธานี'||
                    localString =='พระนครศรีอยุธยา'||
                    localString =='ลพบุรี'||
                    localString =='สมุทรปราการ'||
                    localString =='สมุทรสงคราม'||
                    localString =='สมุทรสาคร'||
                    localString =='สระบุรี'||
                    localString =='สิงห์บุรี'||
                    localString =='สุพรรณบุรี'||
                    localString =='อ่างทอง'){
                    countCentral[dayOfWeek] += 1;

                }else if(localString == 'ตราด'||
                    localString =='ระยอง'||
                    localString =='ฉะเชิงเทรา'||
                    localString =='ปราจีนบุรี'||
                    localString =='สระแก้ว'||
                    localString =='จันทบุรี'||
                    localString =='ชลบุรี'){
                    countNortheastern[dayOfWeek] += 1;

                }else if(localString == 'กาญจนบุรี'||
                    localString =='ราชบุรี'||
                    localString =='เพชรบุรี'||
                    localString =='ประจวบคีรีขันธ์'){
                    countEastern[dayOfWeek] += 1;

                }else if(localString == 'กระบี่'||
                    localString =='ชุมพร'||
                    localString =='ตรัง'||
                    localString =='นครศรีธรรมราช'||
                    localString =='นราธิวาส'||
                    localString =='ปัตตานี'||
                    localString =='พังงา'||
                    localString =='พัทลุง'||
                    localString =='ภูเก็ต'||
                    localString =='ยะลา'||
                    localString =='ระนอง'||
                    localString =='สงขลา'||
                    localString =='สตุล'||
                    localString =='สุราษฎร์ธานี'){
                    countSouthern[dayOfWeek] += 1;
                }
            });

            console.log("● จำนวนผู้สมัครกู้ในแต่ละวันในสัปดาห์ แยกตามสถานที่ทำงาน ");

            console.log('ภาคเหนือ', countNorthern);
            console.log('ภาคกลาง', countCentral);
            console.log('ภาคตะวันออก',  countNortheastern);
            console.log('ภาคตะวันตก', countEastern);
            console.log('ภาคใต้', countSouthern);

            var LocalData = {
                labels: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                series: [
                    { "name": "ภาคเหนือ", "data": [countNorthern.Sun, countNorthern.Mon, countNorthern.Tue, countNorthern.Wed, countNorthern.Thu, countNorthern.Fri, countNorthern.Sat] },
                    { "name": "ภาคกลาง", "data": [countCentral.Sun, countCentral.Mon, countCentral.Tue, countCentral.Wed, countCentral.Thu, countCentral.Fri, countCentral.Sat] },
                    { "name": "ภาคตะวันออก", "data": [countNortheastern.Sun, countNortheastern.Mon, countNortheastern.Tue, countNortheastern.Wed, countNortheastern.Thu, countNortheastern.Fri, countNortheastern.Sat] },
                    { "name": "ภาคตะวันตก", "data": [countEastern.Sun, countEastern.Mon, countEastern.Tue, countEastern.Wed, countEastern.Thu, countEastern.Fri, countEastern.Sat] },
                    { "name": "ภาคใต้", "data": [countSouthern.Sun, countSouthern.Mon, countSouthern.Tue, countSouthern.Wed, countSouthern.Thu, countSouthern.Fri, countSouthern.Sat] }
                ]
            };

            var LocalOptions = {
                lineSmooth: true,
                low: 0,
                high: 50,
                height: "249px",
                area: true,
                axisX: {
                    showGrid: true
                }
            };

            var LocalResponsiveOptions = [
                ['screen and (max-width: 640px)', {
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ];

            Chartist.Bar("#Example4", LocalData, LocalOptions, LocalResponsiveOptions);
        });

        // -------------------- Example Graph - Gender --------------------- //
        var countMale = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        var countFemale = {
            'Mon': 0,
            'Tue': 0,
            'Wed': 0,
            'Thu': 0,
            'Fri': 0,
            'Sat': 0,
            'Sun': 0
        };

        usersRef.on("value", function(res){
            res.forEach(function(user){
                var dateString = user.child('created_at').val();
                var dayOfWeek = $filter('date')(new Date(dateString), 'EEE');
                var genderString = user.child('currentApp/profile/salutation').val();
                if(genderString == 'นาย'){
                    countMale[dayOfWeek] += 1;
                }else if(genderString == 'นางสาว' || genderString == 'นาง' ){
                    countFemale[dayOfWeek] += 1;
                }
            });

            console.log("● จำนวนผู้สมัครกู้ในแต่ละวันในสัปดาห์ แยกตามเพศ ");

            console.log('male', countMale);
            console.log('female', countFemale);


            var GenderData = {
                labels: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                series: [
                    { "name": "Male", "data": [countMale.Sun, countMale.Mon, countMale.Tue, countMale.Wed, countMale.Thu, countMale.Fri, countMale.Sat] },
                    { "name": "Female", "data": [countFemale.Sun, countFemale.Mon, countFemale.Tue, countFemale.Wed, countFemale.Thu, countFemale.Fri, countFemale.Sat] }
                ]
            };

            var GenderOptions = {
                lineSmooth: true,
                low: 0,
                high: 50,
                height: "249px",
                area: true,
                axisX: {
                    showGrid: true
                }
            };

            var GenderResponsiveOptions = [
                ['screen and (max-width: 640px)', {
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ];

            Chartist.Line("#Example5", GenderData, GenderOptions, GenderResponsiveOptions);
        });

        // -------------------- Example Graph - Gender vs. Occupation --------------------- //
        var Male = {
            'employ': 0,
            'Owner': 0
        };

        var Female = {
            'employ': 0,
            'Owner': 0
        };

        usersRef.on("value", function(res){
            res.forEach(function(user){

                var Occupation = user.child('currentApp/occupation').val();
                var Gender = user.child('currentApp/profile/salutation').val();

                if(Occupation == 'employee' && Gender =='นาย'){
                    Male['employ'] += 1;
                }else if(Occupation == 'owner' && Gender =='นาย'){
                    Male['Owner'] += 1;
                }else if(Occupation == 'employee' && Gender =='นาง' || Gender =='นางสาว' ){
                    Female['employ'] += 1;
                }else if(Occupation == 'owner' && Gender =='นาง' || Gender =='นางสาว' ){
                    Female['Owner'] += 1;
                }

            });

            console.log("● จำนวนผู้สมัครกู้ แยกตามเพศและอาชีพ ");

            console.log('Male', Male);
            console.log('Female', Female);

            var GraphData = {
                labels: ['พนักงานประจำ', 'เจ้าของธุรกิจ'],
                series: [
                    { "name": "Male", "data": [Male.employ, Male.Owner] },
                    { "name": "Female", "data": [Female.employ, Female.Owner]}
                ]
            };

            var GraphOptions = {
                lineSmooth: true,
                low: 0,
                high: 130,
                height: "249px",
                area: true,
                axisX: {
                    showGrid: true
                }
            };

            var GraphResponsiveOptions = [
                ['screen and (max-width: 640px)', {
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ];

            Chartist.Bar("#Example6", GraphData, GraphOptions, GraphResponsiveOptions);
        });


    // -------------------- Example Graph - Browser vs. Occupation --------------------- //
    var EmployeeBrowser = {
        'Chrome': 0,
        'Safari': 0,
        'IE': 0,
        'Unknown': 0
    };

    var OwnerBrowser = {
        'Chrome': 0,
        'Safari': 0,
        'IE': 0,
        'Unknown': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var browser = user.child('currentApp/profile/device/browser').val();
            var Occupation = user.child('currentApp/occupation').val();

                    if(Occupation == 'owner' && browser =='chrome'){
                        OwnerBrowser['Chrome'] += 1;
                    }else if(Occupation == 'owner' && browser =='safari'){
                        OwnerBrowser['Safari'] += 1;
                    }else if(Occupation == 'owner' && browser =='ie'){
                        OwnerBrowser['IE'] += 1;
                    }else if(Occupation == 'owner' && browser =='unknown'){
                        OwnerBrowser['Unknown'] += 1;

                    }else if(Occupation == 'employee' && browser =='chrome'){
                        EmployeeBrowser['Chrome'] += 1;
                    }else if(Occupation == 'employee' && browser =='safari'){
                        EmployeeBrowser['Safari'] += 1;
                    }else if(Occupation == 'employee' && browser =='ie'){
                        EmployeeBrowser['IE'] += 1;
                    }else if(Occupation == 'employee' && browser =='unknown') {
                        EmployeeBrowser['Unknown'] += 1;
                    }

                });

        console.log("● จำนวนผู้สมัครกู้ แยกตาม Browser ที่ใช้และอาชีพ ");

        console.log('พนักงานประจำ', EmployeeBrowser);
        console.log('เจ้าของกิจการ', OwnerBrowser);


        var BrowserAndCareerData = {
            labels: ['Chrome', 'Safari', 'IE', 'Unknown'],
            series: [
                { "name": "พนักงานประจำ", "data": [EmployeeBrowser.Chrome,EmployeeBrowser.Safari,EmployeeBrowser.IE,EmployeeBrowser.Unknown]},
                { "name": "เจ้าของกิจการ", "data": [OwnerBrowser.Chrome,OwnerBrowser.Safari,OwnerBrowser.IE,OwnerBrowser.Unknown]}
            ]
        };

        var BrowserAndCareerOptions = {
            lineSmooth: true,
            low: 0,
            high: 170,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var BrowserAndCareerResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar("#Example7", BrowserAndCareerData, BrowserAndCareerOptions, BrowserAndCareerResponsive);
    });

    // -------------------- Example Graph - Loan_type vs. Occupation --------------------- //

    var EmployeeType = {
        'Cash': 0,
        'House': 0,
        'Card': 0,
        'Kaifak': 0,
        'Car_for_cash':0
    };

    var OwnerType = {
        'Cash': 0,
        'House': 0,
        'Card': 0,
        'Kaifak': 0,
        'Car_for_cash':0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var loan = user.child('currentApp/loan').val();
            var Occupation = user.child('currentApp/occupation').val();

            if(Occupation == 'owner' && loan =='cash'){
                OwnerType['Cash'] += 1;
            }else if(Occupation == 'owner' && loan =='house'){
                OwnerType['House'] += 1;
            }else if(Occupation == 'owner' && loan =='card'){
                OwnerType['Card'] += 1;
            }else if(Occupation == 'owner' && loan =='kaifak'){
                OwnerType['Kaifak'] += 1;
            }else if(Occupation == 'owner' && loan =='car_for_cash'){
                OwnerType['Car_for_cash'] += 1;

            }else if(Occupation == 'employee' && loan =='cash'){
                EmployeeType['Cash'] += 1;
            }else if(Occupation == 'employee' && loan =='house'){
                EmployeeType['House'] += 1;
            }else if(Occupation == 'employee' && loan =='card'){
                EmployeeType['Card'] += 1;
            }else if(Occupation == 'employee' && loan =='kaifak') {
                EmployeeType['Kaifak'] += 1;
            }else if(Occupation == 'employee' && loan =='car_for_cash') {
                EmployeeType['Car_for_cash'] += 1;
            }
        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามประเภททการกู้และอาชีพ ");

        console.log('พนักงานประจำ', EmployeeType);
        console.log('เจ้าของกิจการ', OwnerType);


        var LoanData = {
            labels: ['Cash', 'House', 'Card', 'Kaifak', 'Car for cash'],
            series: [
                { "name": "พนักงานประจำ", "data": [EmployeeType.Cash,EmployeeType.House,EmployeeType.Card,EmployeeType.Kaifak,EmployeeType.Car_for_cash]},
                { "name": "เจ้าของกิจการ", "data": [OwnerType.Cash,OwnerType.House,OwnerType.Card,OwnerType.Kaifak,OwnerType.Car_for_cash]}
            ]
        };

        var LoanOptions = {
            lineSmooth: false,
            low: 0,
            high: 80,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var LoanResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Line("#Example8", LoanData, LoanOptions, LoanResponsive);
    });

// -------------------- Example Graph - Income vs. Occupation --------------------- //

    var EmployIncome = {
        'low_20k': 0,
        'more_20k': 0,
        'more_30k': 0,
        'more_40k': 0,
        'more_50k': 0
    };

    var OwnerIncome = {
        'low_20k': 0,
        'more_20k': 0,
        'more_30k': 0,
        'more_40k': 0,
        'more_50k': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var income = user.child('currentApp/profile/income').val();
            var Occupation = user.child('currentApp/occupation').val();

            if(Occupation == 'owner' && income <= '19999'){
                OwnerIncome['low_20k'] += 1;
            }else if(Occupation == 'owner' && income >= '20000') {
                OwnerIncome['more_20k'] += 1;
            }else if(Occupation == 'owner' && income >= '30000'){
                OwnerIncome['more_30k'] += 1;
            }else if(Occupation == 'owner' && income >= '40000'){
                OwnerIncome['more_40k'] += 1;
            }else if(Occupation == 'owner' && income >= '50000'){
                OwnerIncome['more_50k'] += 1;

            }else if(Occupation == 'employee' && income < '19999'){
                EmployIncome['low_20k'] += 1;
            }else if(Occupation == 'employee' && income > '20000'){
                EmployIncome['more_20k'] += 1;
            }else if(Occupation == 'employee' && income > '30000'){
                EmployIncome['more_30k'] += 1;
            }else if(Occupation == 'employee' && income > '40000') {
                EmployIncome['more_40k'] += 1;
            }else if(Occupation == 'employee' && income > '50000') {
                EmployIncome['more_50k'] += 1;
            }
        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามรายได้และอาชีพ ");

        console.log('พนักงานประจำ', EmployIncome);
        console.log('เจ้าของกิจการ', OwnerIncome);


        var IncomeData = {
            labels: ['ต่ำกว่า 20k', '20k ขึ้นไป', '30k ขึ้นไป', '40k ขึ้นไป', '50k ขึ้นไป '],
            series: [
                { "name": "พนักงานประจำ", "data": [EmployIncome.low_20k,EmployIncome.more_20k,EmployIncome.more_30k,EmployIncome.more_40k,EmployIncome.more_50k]},
                { "name": "เจ้าของกิจการ", "data": [OwnerIncome.low_20k,OwnerIncome.more_20k,OwnerIncome.more_30k,OwnerIncome.more_40k,OwnerIncome.more_50k]}
            ]
        };

        var IncomeOptions = {
            lineSmooth: false,
            low: 0,
            high: 160,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var IncomeResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Line("#Example9", IncomeData, IncomeOptions,IncomeResponsive);
    });

    // -------------------- Example Graph - Loan_money vs. Occupation --------------------- //

    var EmployLoan = {
        'low_20k': 0,
        'more_20k': 0,
        'more_30k': 0,
        'more_40k': 0,
        'more_50k': 0
    };

    var OwnerLoan = {
        'low_20k': 0,
        'more_20k': 0,
        'more_30k': 0,
        'more_40k': 0,
        'more_50k': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var Loan = user.child('currentApp/loan').val();
            var Occupation = user.child('currentApp/occupation').val();

            if(Occupation == 'owner' && Loan <= '19999'){
                OwnerLoan['low_20k'] += 1;
            }else if(Occupation == 'owner' && Loan >= '20000') {
                OwnerLoan['more_20k'] += 1;
            }else if(Occupation == 'owner' && Loan >= '30000'){
                OwnerLoan['more_30k'] += 1;
            }else if(Occupation == 'owner' && Loan >= '40000'){
                OwnerLoan['more_40k'] += 1;
            }else if(Occupation == 'owner' && Loan >= '50000'){
                OwnerLoan['more_50k'] += 1;

            }else if(Occupation == 'employee' && Loan <= '19999'){
                EmployLoan['low_20k'] += 1;
            }else if(Occupation == 'employee' && Loan >= '20000'){
                EmployLoan['more_20k'] += 1;
            }else if(Occupation == 'employee' && Loan >= '30000'){
                EmployLoan['more_30k'] += 1;
            }else if(Occupation == 'employee' && Loan >= '40000') {
                EmployLoan['more_40k'] += 1;
            }else if(Occupation == 'employee' && Loan >= '50000') {
                EmployLoan['more_50k'] += 1;
            }
        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามจำนวนเงินที่ขอกู้และอาชีพ ");

        console.log('พนักงานประจำ', EmployLoan);
        console.log('เจ้าของกิจการ', OwnerLoan);


        var LoanData = {
            labels: ['ต่ำกว่า 20k', '20k ขึ้นไป', '30k ขึ้นไป', '40k ขึ้นไป', '50k ขึ้นไป '],
            series: [
                { "name": "พนักงานประจำ", "data": [EmployLoan.low_20k,EmployLoan.more_20k,EmployLoan.more_30k,EmployLoan.more_40k,EmployLoan.more_50k]},
                { "name": "เจ้าของกิจการ", "data": [OwnerLoan.low_20k,OwnerLoan.more_20k,OwnerLoan.more_30k,OwnerLoan.more_40k,OwnerLoan.more_50k]}
            ]
        };

        var LoanOptions = {
            lineSmooth: false,
            low: 0,
            high: 160,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var LoanResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Line("#Example10", LoanData, LoanOptions,LoanResponsive);
    });

// -------------------- Example Graph - Income vs. Gender --------------------- //

    var MaleIn = {
        'low_20k': 0,
        'more_20k': 0,
        'more_30k': 0,
        'more_40k': 0,
        'more_50k': 0
    };

    var FemaleIn = {
        'low_20k': 0,
        'more_20k': 0,
        'more_30k': 0,
        'more_40k': 0,
        'more_50k': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var income = user.child('currentApp/profile/income').val();
            var Gender = user.child('currentApp/profile/salutation').val();

            if(Gender == 'นาย' && income <= '19999'){
                MaleIn['low_20k'] += 1;
            }else if(Gender == 'นาย' && income >= '20000') {
                MaleIn['more_20k'] += 1;
            }else if(Gender == 'นาย' && income >= '30000'){
                MaleIn['more_30k'] += 1;
            }else if(Gender == 'นาย' && income >= '40000'){
                MaleIn['more_40k'] += 1;
            }else if(Gender == 'นาย' && income >= '50000'){
                MaleIn['more_50k'] += 1;

            }else if(Gender == 'นางสาว' || Gender == 'นาง' && income <= '19999'){
                FemaleIn['low_20k'] += 1;
            }else if(Gender == 'นางสาว' || Gender == 'นาง'&& income >= '20000'){
                FemaleIn['more_20k'] += 1;
            }else if(Gender == 'นางสาว' || Gender == 'นาง' && income >= '30000'){
                FemaleIn['more_30k'] += 1;
            }else if(Gender == 'นางสาว' || Gender == 'นาง' && income >= '40000') {
                FemaleIn['more_40k'] += 1;
            }else if(Gender == 'นางสาว' || Gender == 'นาง' && income >= '50000') {
                FemaleIn['more_50k'] += 1;
            }
        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามเพศและรายได้ ");

        console.log('เพศชาย', MaleIn);
        console.log('เพศหญิง', FemaleIn);


        var GenInData = {
            labels: ['ต่ำกว่า 20k', '20k ขึ้นไป', '30k ขึ้นไป', '40k ขึ้นไป', '50k ขึ้นไป '],
            series: [
                { "name": "เพศชาย", "data": [MaleIn.low_20k,MaleIn.more_20k,MaleIn.more_30k,MaleIn.more_40k,MaleIn.more_50k]},
                { "name": "เพศหญิง", "data": [FemaleIn.low_20k,FemaleIn.more_20k,FemaleIn.more_30k,FemaleIn.more_40k,FemaleIn.more_50k]}
            ]
        };

        var GenInOptions = {
            lineSmooth: false,
            low: 0,
            high: 130,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var GenInResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar("#Example11", GenInData, GenInOptions,GenInResponsive);
    });

    // -------------------- Example Graph - Os vs. Gender --------------------- //

    var MaleOS = {
        'Mac': 0,
        'ios': 0,
        'Android': 0,
        'Windows': 0
    };

    var FemaleOS = {
        'Mac': 0,
        'ios': 0,
        'Android': 0,
        'Windows': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var OS = user.child('currentApp/profile/device/os').val();
            var Gender = user.child('currentApp/profile/salutation').val();

            if(Gender == 'นาย' && OS == 'mac'){
                MaleOS['Mac'] += 1;
            }else if(Gender == 'นาย' && OS == 'ios') {
                MaleOS['ios'] += 1;
            }else if(Gender == 'นาย' && OS == 'android'){
                MaleOS['Android'] += 1;
            }else if(Gender == 'นาย' && OS == 'windows'){
                MaleOS['Windows'] += 1;


            }else if(Gender == 'นางสาว' || Gender == 'นาง'&& OS == 'mac'){
                FemaleOS['Mac'] += 1;
            }else if(Gender == 'นางสาว' || Gender == 'นาง' && OS == 'ios'){
                FemaleOS['ios'] += 1;
            }else if(Gender == 'นางสาว' || Gender == 'นาง' && OS == 'android') {
                FemaleOS['Android'] += 1;
            }else if(Gender == 'นางสาว' || Gender == 'นาง' && OS == 'windows') {
                FemaleOS['Windows'] += 1;
            }
        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามเพศและ OS ที่ใช้ ");

        console.log('เพศชาย', MaleOS);
        console.log('เพศหญิง', FemaleOS);


        var GenOSData = {
            labels: ['Mac', 'ios', 'Android', 'Windows'],
            series: [
                { "name": "เพศชาย", "data": [MaleOS.Mac,MaleOS.ios,MaleOS.Android,MaleOS.Windows]},
                { "name": "เพศหญิง", "data": [FemaleOS.Mac,FemaleOS.ios,FemaleOS.Android,FemaleOS.Windows]}
            ]
        };

        var GenOSOptions = {
            lineSmooth: false,
            low: 0,
            high: 130,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var GenOSResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar("#Example12", GenOSData, GenOSOptions,GenOSResponsive);
    });

    // -------------------- Example Graph - Os vs. Work place --------------------- //

    var Northern = {
        'Mac': 0,
        'ios': 0,
        'Android': 0,
        'Windows': 0
    };

    var Central = {
        'Mac': 0,
        'ios': 0,
        'Android': 0,
        'Windows': 0
    };

    var Northeastern = {
        'Mac': 0,
        'ios': 0,
        'Android': 0,
        'Windows': 0
    };

    var Eastern = {
        'Mac': 0,
        'ios': 0,
        'Android': 0,
        'Windows': 0
    };

    var Southern = {
        'Mac': 0,
        'ios': 0,
        'Android': 0,
        'Windows': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var OS = user.child('currentApp/profile/device/os').val();
            var localString = user.child('currentApp/profile/workPlace').val();

            if(localString == 'เชียงใหม่'||
                localString =='เชียงราย'||
                localString =='น่าน'||
                localString =='พะเยา'||
                localString =='แพร่'||
                localString =='แม่ฮ่องสอน'||
                localString =='ลำพูน'||
                localString =='ลำปาง'||
                localString =='ตาก'||
                localString =='นครสวรรค์'||
                localString =='พิจิตร'||
                localString =='พิษณุโลก'||
                localString =='อุตรดิตถ์'||
                localString =='อุทัยธานี'||
                localString =='เพชรบูรณ์'||
                localString =='สุโขทัย'||
                localString =='กำแพงเพชร' && OS == 'mac'){
                Northern['Mac'] += 1;

            }else if(localString == 'กทม'||
                localString =='กรุงเทพฯ'||
                localString =='กรุงเทพ'||
                localString =='ชัยนาท'||
                localString =='นครนายก'||
                localString =='นครปฐม'||
                localString =='นนทบุรี'||
                localString =='ปทุมธานี'||
                localString =='พระนครศรีอยุธยา'||
                localString =='ลพบุรี'||
                localString =='สมุทรปราการ'||
                localString =='สมุทรสงคราม'||
                localString =='สมุทรสาคร'||
                localString =='สระบุรี'||
                localString =='สิงห์บุรี'||
                localString =='สุพรรณบุรี'||
                localString =='อ่างทอง' && OS == 'mac'){
                Central['Mac'] += 1;

            }else if(localString == 'ตราด'||
                localString =='ระยอง'||
                localString =='ฉะเชิงเทรา'||
                localString =='ปราจีนบุรี'||
                localString =='สระแก้ว'||
                localString =='จันทบุรี'||
                localString =='ชลบุรี' && OS == 'mac'){
                Northeastern['Mac'] += 1;

            }else if(localString == 'กาญจนบุรี'||
                localString =='ราชบุรี'||
                localString =='เพชรบุรี'||
                localString =='ประจวบคีรีขันธ์' && OS == 'mac'){
                Eastern['Mac'] += 1;

            }else if(localString == 'กระบี่'||
                localString =='ชุมพร'||
                localString =='ตรัง'||
                localString =='นครศรีธรรมราช'||
                localString =='นราธิวาส'||
                localString =='ปัตตานี'||
                localString =='พังงา'||
                localString =='พัทลุง'||
                localString =='ภูเก็ต'||
                localString =='ยะลา'||
                localString =='ระนอง'||
                localString =='สงขลา'||
                localString =='สตุล'||
                localString =='สุราษฎร์ธานี' && OS == 'mac'){
                Southern['Mac'] += 1;


            } else if (localString == 'เชียงใหม่'||
                localString =='เชียงราย'||
                localString =='น่าน'||
                localString =='พะเยา'||
                localString =='แพร่'||
                localString =='แม่ฮ่องสอน'||
                localString =='ลำพูน'||
                localString =='ลำปาง'||
                localString =='ตาก'||
                localString =='นครสวรรค์'||
                localString =='พิจิตร'||
                localString =='พิษณุโลก'||
                localString =='อุตรดิตถ์'||
                localString =='อุทัยธานี'||
                localString =='เพชรบูรณ์'||
                localString =='สุโขทัย'||
                localString =='กำแพงเพชร' && OS == 'ios'){
                Northern['ios'] += 1;

            }else if(localString == 'กทม'||
                localString =='กรุงเทพฯ'||
                localString =='กรุงเทพ'||
                localString =='ชัยนาท'||
                localString =='นครนายก'||
                localString =='นครปฐม'||
                localString =='นนทบุรี'||
                localString =='ปทุมธานี'||
                localString =='พระนครศรีอยุธยา'||
                localString =='ลพบุรี'||
                localString =='สมุทรปราการ'||
                localString =='สมุทรสงคราม'||
                localString =='สมุทรสาคร'||
                localString =='สระบุรี'||
                localString =='สิงห์บุรี'||
                localString =='สุพรรณบุรี'||
                localString =='อ่างทอง' && OS == 'ios'){
                Central['ios'] += 1;

            }else if(localString == 'ตราด'||
                localString =='ระยอง'||
                localString =='ฉะเชิงเทรา'||
                localString =='ปราจีนบุรี'||
                localString =='สระแก้ว'||
                localString =='จันทบุรี'||
                localString =='ชลบุรี' && OS == 'ios'){
                Northeastern['ios'] += 1;

            }else if(localString == 'กาญจนบุรี'||
                localString =='ราชบุรี'||
                localString =='เพชรบุรี'||
                localString =='ประจวบคีรีขันธ์' && OS == 'ios'){
                Eastern['ios'] += 1;

            }else if(localString == 'กระบี่'||
                localString =='ชุมพร'||
                localString =='ตรัง'||
                localString =='นครศรีธรรมราช'||
                localString =='นราธิวาส'||
                localString =='ปัตตานี'||
                localString =='พังงา'||
                localString =='พัทลุง'||
                localString =='ภูเก็ต'||
                localString =='ยะลา'||
                localString =='ระนอง'||
                localString =='สงขลา'||
                localString =='สตุล'||
                localString =='สุราษฎร์ธานี' && OS == 'ios') {
                Southern['ios'] += 1;


            } else if (localString == 'เชียงใหม่'||
            localString =='เชียงราย'||
            localString =='น่าน'||
            localString =='พะเยา'||
            localString =='แพร่'||
            localString =='แม่ฮ่องสอน'||
            localString =='ลำพูน'||
            localString =='ลำปาง'||
            localString =='ตาก'||
            localString =='นครสวรรค์'||
            localString =='พิจิตร'||
            localString =='พิษณุโลก'||
            localString =='อุตรดิตถ์'||
            localString =='อุทัยธานี'||
            localString =='เพชรบูรณ์'||
            localString =='สุโขทัย'||
            localString =='กำแพงเพชร' && OS == 'android'){
            Northern['Android'] += 1;

        }else if(localString == 'กทม'||
            localString =='กรุงเทพฯ'||
            localString =='กรุงเทพ'||
            localString =='ชัยนาท'||
            localString =='นครนายก'||
            localString =='นครปฐม'||
            localString =='นนทบุรี'||
            localString =='ปทุมธานี'||
            localString =='พระนครศรีอยุธยา'||
            localString =='ลพบุรี'||
            localString =='สมุทรปราการ'||
            localString =='สมุทรสงคราม'||
            localString =='สมุทรสาคร'||
            localString =='สระบุรี'||
            localString =='สิงห์บุรี'||
            localString =='สุพรรณบุรี'||
            localString =='อ่างทอง' && OS == 'android'){
            Central['Android'] += 1;

        }else if(localString == 'ตราด'||
            localString =='ระยอง'||
            localString =='ฉะเชิงเทรา'||
            localString =='ปราจีนบุรี'||
            localString =='สระแก้ว'||
            localString =='จันทบุรี'||
            localString =='ชลบุรี' && OS == 'android'){
            Northeastern['Android'] += 1;

        }else if(localString == 'กาญจนบุรี'||
            localString =='ราชบุรี'||
            localString =='เพชรบุรี'||
            localString =='ประจวบคีรีขันธ์' && OS == 'android'){
            Eastern['Android'] += 1;

        }else if(localString == 'กระบี่'||
            localString =='ชุมพร'||
            localString =='ตรัง'||
            localString =='นครศรีธรรมราช'||
            localString =='นราธิวาส'||
            localString =='ปัตตานี'||
            localString =='พังงา'||
            localString =='พัทลุง'||
            localString =='ภูเก็ต'||
            localString =='ยะลา'||
            localString =='ระนอง'||
            localString =='สงขลา'||
            localString =='สตุล'||
            localString =='สุราษฎร์ธานี' && OS == 'android') {
            Southern['Android'] += 1;


        }else if (localString == 'เชียงใหม่'||
                localString =='เชียงราย'||
                localString =='น่าน'||
                localString =='พะเยา'||
                localString =='แพร่'||
                localString =='แม่ฮ่องสอน'||
                localString =='ลำพูน'||
                localString =='ลำปาง'||
                localString =='ตาก'||
                localString =='นครสวรรค์'||
                localString =='พิจิตร'||
                localString =='พิษณุโลก'||
                localString =='อุตรดิตถ์'||
                localString =='อุทัยธานี'||
                localString =='เพชรบูรณ์'||
                localString =='สุโขทัย'||
                localString =='กำแพงเพชร' && OS == 'windows'){
                Northern['Windows'] += 1;

            }else if(localString == 'กทม'||
                localString =='กรุงเทพฯ'||
                localString =='กรุงเทพ'||
                localString =='ชัยนาท'||
                localString =='นครนายก'||
                localString =='นครปฐม'||
                localString =='นนทบุรี'||
                localString =='ปทุมธานี'||
                localString =='พระนครศรีอยุธยา'||
                localString =='ลพบุรี'||
                localString =='สมุทรปราการ'||
                localString =='สมุทรสงคราม'||
                localString =='สมุทรสาคร'||
                localString =='สระบุรี'||
                localString =='สิงห์บุรี'||
                localString =='สุพรรณบุรี'||
                localString =='อ่างทอง' && OS == 'windows'){
                Central['Windows'] += 1;

            }else if(localString == 'ตราด'||
                localString =='ระยอง'||
                localString =='ฉะเชิงเทรา'||
                localString =='ปราจีนบุรี'||
                localString =='สระแก้ว'||
                localString =='จันทบุรี'||
                localString =='ชลบุรี' && OS == 'windows'){
                Northeastern['Windows'] += 1;

            }else if(localString == 'กาญจนบุรี'||
                localString =='ราชบุรี'||
                localString =='เพชรบุรี'||
                localString =='ประจวบคีรีขันธ์' && OS == 'windows'){
                Eastern['Windows'] += 1;

            }else if(localString == 'กระบี่'||
                localString =='ชุมพร'||
                localString =='ตรัง'||
                localString =='นครศรีธรรมราช'||
                localString =='นราธิวาส'||
                localString =='ปัตตานี'||
                localString =='พังงา'||
                localString =='พัทลุง'||
                localString =='ภูเก็ต'||
                localString =='ยะลา'||
                localString =='ระนอง'||
                localString =='สงขลา'||
                localString =='สตุล'||
                localString =='สุราษฎร์ธานี' && OS == 'windows') {
                Southern['Windows'] += 1;
            }

        });

        console.log("● จำนวนผู้สมัครกู้ แยกตาม OS ที่ใช้และสถานที่ทำงาน ");

        console.log('ภาคเหนือ', Northern);
        console.log('ภาคกลาง', Central);
        console.log('ภาคตะวันออก', Northeastern);
        console.log('ภาคตะวันตก', Eastern);
        console.log('ภาคใต้', Southern);

        var WorkOSData = {
            labels: ['Mac', 'ios', 'Android', 'Windows'],
            series: [
                { "name": "ภาคเหนือ", "data": [Northern.Mac, Northern.ios, Northern.Android, Northern.Windows]},
                { "name": "ภาคกลาง", "data": [Central.Mac, Central.ios, Central.Android, Central.Windows]},
                { "name": "ภาคตะวันออก", "data": [Northeastern.Mac, Northeastern.ios, Northeastern.Android, Northeastern.Windows]},
                { "name": "ภาคตะวันตก", "data": [Eastern.Mac, Eastern.ios, Eastern.Android, Eastern.Windows]},
                { "name": "ภาคใต้", "data": [Southern.Mac, Southern.ios, Southern.Android, Southern.Windows]}
            ]
        };

        var WorkOSOptions = {
            lineSmooth: false,
            low: 0,
            high: 90,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var WorkOSResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar("#Example13", WorkOSData, WorkOSOptions,WorkOSResponsive);
    });

    // -------------------- Example Graph - Gender vs. Work place --------------------- //

    var northern = {
        'Male': 0,
        'Female': 0
    };

    var central = {
        'Male': 0,
        'Female': 0
    };

    var northeastern = {
        'Male': 0,
        'Female': 0
    };

    var eastern = {
        'Male': 0,
        'Female': 0
    };

    var southern = {
        'Male': 0,
        'Female': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var gender = user.child('currentApp/profile/salutation').val();
            var localString = user.child('currentApp/profile/workPlace').val();

            if(localString == 'เชียงใหม่'||
                localString =='เชียงราย'||
                localString =='น่าน'||
                localString =='พะเยา'||
                localString =='แพร่'||
                localString =='แม่ฮ่องสอน'||
                localString =='ลำพูน'||
                localString =='ลำปาง'||
                localString =='ตาก'||
                localString =='นครสวรรค์'||
                localString =='พิจิตร'||
                localString =='พิษณุโลก'||
                localString =='อุตรดิตถ์'||
                localString =='อุทัยธานี'||
                localString =='เพชรบูรณ์'||
                localString =='สุโขทัย'||
                localString =='กำแพงเพชร' && gender == 'นาย'){
                northern['Male'] += 1;

            }else if(localString == 'กทม'||
                localString =='กรุงเทพฯ'||
                localString =='กรุงเทพ'||
                localString =='ชัยนาท'||
                localString =='นครนายก'||
                localString =='นครปฐม'||
                localString =='นนทบุรี'||
                localString =='ปทุมธานี'||
                localString =='พระนครศรีอยุธยา'||
                localString =='ลพบุรี'||
                localString =='สมุทรปราการ'||
                localString =='สมุทรสงคราม'||
                localString =='สมุทรสาคร'||
                localString =='สระบุรี'||
                localString =='สิงห์บุรี'||
                localString =='สุพรรณบุรี'||
                localString =='อ่างทอง' && gender == 'นาย'){
                central['Male'] += 1;

            }else if(localString == 'ตราด'||
                localString =='ระยอง'||
                localString =='ฉะเชิงเทรา'||
                localString =='ปราจีนบุรี'||
                localString =='สระแก้ว'||
                localString =='จันทบุรี'||
                localString =='ชลบุรี' && gender == 'นาย'){
                northeastern['Male'] += 1;

            }else if(localString == 'กาญจนบุรี'||
                localString =='ราชบุรี'||
                localString =='เพชรบุรี'||
                localString =='ประจวบคีรีขันธ์' && gender == 'นาย'){
                eastern['Male'] += 1;

            }else if(localString == 'กระบี่'||
                localString =='ชุมพร'||
                localString =='ตรัง'||
                localString =='นครศรีธรรมราช'||
                localString =='นราธิวาส'||
                localString =='ปัตตานี'||
                localString =='พังงา'||
                localString =='พัทลุง'||
                localString =='ภูเก็ต'||
                localString =='ยะลา'||
                localString =='ระนอง'||
                localString =='สงขลา'||
                localString =='สตุล'||
                localString =='สุราษฎร์ธานี' && gender == 'นาย'){
                southern['Male'] += 1;


            } else if (localString == 'เชียงใหม่'||
                localString =='เชียงราย'||
                localString =='น่าน'||
                localString =='พะเยา'||
                localString =='แพร่'||
                localString =='แม่ฮ่องสอน'||
                localString =='ลำพูน'||
                localString =='ลำปาง'||
                localString =='ตาก'||
                localString =='นครสวรรค์'||
                localString =='พิจิตร'||
                localString =='พิษณุโลก'||
                localString =='อุตรดิตถ์'||
                localString =='อุทัยธานี'||
                localString =='เพชรบูรณ์'||
                localString =='สุโขทัย'||
                localString =='กำแพงเพชร' && gender == 'นาง'|| gender == 'นางสาว' ){
                northern['Female'] += 1;

            }else if(localString == 'กทม'||
                localString =='กรุงเทพฯ'||
                localString =='กรุงเทพ'||
                localString =='ชัยนาท'||
                localString =='นครนายก'||
                localString =='นครปฐม'||
                localString =='นนทบุรี'||
                localString =='ปทุมธานี'||
                localString =='พระนครศรีอยุธยา'||
                localString =='ลพบุรี'||
                localString =='สมุทรปราการ'||
                localString =='สมุทรสงคราม'||
                localString =='สมุทรสาคร'||
                localString =='สระบุรี'||
                localString =='สิงห์บุรี'||
                localString =='สุพรรณบุรี'||
                localString =='อ่างทอง' && gender == 'นาง'|| gender == 'นางสาว'){
                central['Female'] += 1;

            }else if(localString == 'ตราด'||
                localString =='ระยอง'||
                localString =='ฉะเชิงเทรา'||
                localString =='ปราจีนบุรี'||
                localString =='สระแก้ว'||
                localString =='จันทบุรี'||
                localString =='ชลบุรี' && gender == 'นาง'|| gender == 'นางสาว'){
                northeastern['Female'] += 1;

            }else if(localString == 'กาญจนบุรี'||
                localString =='ราชบุรี'||
                localString =='เพชรบุรี'||
                localString =='ประจวบคีรีขันธ์' && gender == 'นาง'|| gender == 'นางสาว'){
                eastern['Female'] += 1;

            }else if(localString == 'กระบี่'||
                localString =='ชุมพร'||
                localString =='ตรัง'||
                localString =='นครศรีธรรมราช'||
                localString =='นราธิวาส'||
                localString =='ปัตตานี'||
                localString =='พังงา'||
                localString =='พัทลุง'||
                localString =='ภูเก็ต'||
                localString =='ยะลา'||
                localString =='ระนอง'||
                localString =='สงขลา'||
                localString =='สตุล'||
                localString =='สุราษฎร์ธานี' && gender == 'นาง'|| gender == 'นางสาว') {
                southern['Female'] += 1;
            }

        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามเพศและสถานที่ทำงาน ");

        console.log('ภาคเหนือ', northern);
        console.log('ภาคกลาง', central);
        console.log('ภาคตะวันออก', northeastern);
        console.log('ภาคตะวันตก', eastern);
        console.log('ภาคใต้', southern);

        var WorkGenderData = {
            labels: ['เพศชาย', 'เพศหญิง'],
            series: [
                { "name": "ภาคเหนือ", "data": [northern.Male, northern.Female]},
                { "name": "ภาคกลาง", "data": [central.Male, central.Female]},
                { "name": "ภาคตะวันออก", "data": [northeastern.Male, northeastern.Female]},
                { "name": "ภาคตะวันตก", "data": [eastern.Male, eastern.Female]},
                { "name": "ภาคใต้", "data": [southern.Male, southern.Female]}
            ]
        };

        var WorkGenderOptions = {
            lineSmooth: false,
            low: 0,
            high: 90,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var WorkGenderResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar("#Example14", WorkGenderData, WorkGenderOptions, WorkGenderResponsive);
    });

    // -------------------- Example Graph - Gender vs. Work place --------------------- //

    var north = {
        'Owner': 0,
        'Employee': 0
    };

    var centrals = {
        'Owner': 0,
        'Employee': 0
    };

    var northerns = {
        'Owner': 0,
        'Employee': 0
    };

    var east = {
        'Owner': 0,
        'Employee': 0
    };

    var south = {
        'Owner': 0,
        'Employee': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var Occupation = user.child('currentApp/occupation').val();
            var localString = user.child('currentApp/profile/workPlace').val();

            if(localString == 'เชียงใหม่'||
                localString =='เชียงราย'||
                localString =='น่าน'||
                localString =='พะเยา'||
                localString =='แพร่'||
                localString =='แม่ฮ่องสอน'||
                localString =='ลำพูน'||
                localString =='ลำปาง'||
                localString =='ตาก'||
                localString =='นครสวรรค์'||
                localString =='พิจิตร'||
                localString =='พิษณุโลก'||
                localString =='อุตรดิตถ์'||
                localString =='อุทัยธานี'||
                localString =='เพชรบูรณ์'||
                localString =='สุโขทัย'||
                localString =='กำแพงเพชร' && Occupation == 'owner'){
                north['Owner'] += 1;

            }else if(localString == 'กทม'||
                localString =='กรุงเทพฯ'||
                localString =='กรุงเทพ'||
                localString =='ชัยนาท'||
                localString =='นครนายก'||
                localString =='นครปฐม'||
                localString =='นนทบุรี'||
                localString =='ปทุมธานี'||
                localString =='พระนครศรีอยุธยา'||
                localString =='ลพบุรี'||
                localString =='สมุทรปราการ'||
                localString =='สมุทรสงคราม'||
                localString =='สมุทรสาคร'||
                localString =='สระบุรี'||
                localString =='สิงห์บุรี'||
                localString =='สุพรรณบุรี'||
                localString =='อ่างทอง' && Occupation == 'owner'){
                centrals['Owner'] += 1;

            }else if(localString == 'ตราด'||
                localString =='ระยอง'||
                localString =='ฉะเชิงเทรา'||
                localString =='ปราจีนบุรี'||
                localString =='สระแก้ว'||
                localString =='จันทบุรี'||
                localString =='ชลบุรี' && Occupation == 'owner'){
                northerns['Owner'] += 1;

            }else if(localString == 'กาญจนบุรี'||
                localString =='ราชบุรี'||
                localString =='เพชรบุรี'||
                localString =='ประจวบคีรีขันธ์' && Occupation == 'owner'){
                east['Owner'] += 1;

            }else if(localString == 'กระบี่'||
                localString =='ชุมพร'||
                localString =='ตรัง'||
                localString =='นครศรีธรรมราช'||
                localString =='นราธิวาส'||
                localString =='ปัตตานี'||
                localString =='พังงา'||
                localString =='พัทลุง'||
                localString =='ภูเก็ต'||
                localString =='ยะลา'||
                localString =='ระนอง'||
                localString =='สงขลา'||
                localString =='สตุล'||
                localString =='สุราษฎร์ธานี' && Occupation == 'owner'){
                south['Owner'] += 1;


            } else if (localString == 'เชียงใหม่'||
                localString =='เชียงราย'||
                localString =='น่าน'||
                localString =='พะเยา'||
                localString =='แพร่'||
                localString =='แม่ฮ่องสอน'||
                localString =='ลำพูน'||
                localString =='ลำปาง'||
                localString =='ตาก'||
                localString =='นครสวรรค์'||
                localString =='พิจิตร'||
                localString =='พิษณุโลก'||
                localString =='อุตรดิตถ์'||
                localString =='อุทัยธานี'||
                localString =='เพชรบูรณ์'||
                localString =='สุโขทัย'||
                localString =='กำแพงเพชร' && Occupation == 'employee'){
                north['Employee'] += 1;

            }else if(localString == 'กทม'||
                localString =='กรุงเทพฯ'||
                localString =='กรุงเทพ'||
                localString =='ชัยนาท'||
                localString =='นครนายก'||
                localString =='นครปฐม'||
                localString =='นนทบุรี'||
                localString =='ปทุมธานี'||
                localString =='พระนครศรีอยุธยา'||
                localString =='ลพบุรี'||
                localString =='สมุทรปราการ'||
                localString =='สมุทรสงคราม'||
                localString =='สมุทรสาคร'||
                localString =='สระบุรี'||
                localString =='สิงห์บุรี'||
                localString =='สุพรรณบุรี'||
                localString =='อ่างทอง' && Occupation == 'employee'){
                centrals['Employee'] += 1;

            }else if(localString == 'ตราด'||
                localString =='ระยอง'||
                localString =='ฉะเชิงเทรา'||
                localString =='ปราจีนบุรี'||
                localString =='สระแก้ว'||
                localString =='จันทบุรี'||
                localString =='ชลบุรี' && Occupation == 'employee'){
                northerns['Employee'] += 1;

            }else if(localString == 'กาญจนบุรี'||
                localString =='ราชบุรี'||
                localString =='เพชรบุรี'||
                localString =='ประจวบคีรีขันธ์' && Occupation == 'employee'){
                east['Employee'] += 1;

            }else if(localString == 'กระบี่'||
                localString =='ชุมพร'||
                localString =='ตรัง'||
                localString =='นครศรีธรรมราช'||
                localString =='นราธิวาส'||
                localString =='ปัตตานี'||
                localString =='พังงา'||
                localString =='พัทลุง'||
                localString =='ภูเก็ต'||
                localString =='ยะลา'||
                localString =='ระนอง'||
                localString =='สงขลา'||
                localString =='สตุล'||
                localString =='สุราษฎร์ธานี' && Occupation == 'employee') {
                south['Employee'] += 1;
            }

        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามอาชีพและสถานที่ทำงาน ");

        console.log('ภาคเหนือ', north);
        console.log('ภาคกลาง', centrals);
        console.log('ภาคตะวันออก', northerns);
        console.log('ภาคตะวันตก', east);
        console.log('ภาคใต้', south);

        var WorkOccupData = {
            labels: ['เจ้าของกิจการ', 'พนักงานประจำ'],
            series: [
                { "name": "ภาคเหนือ", "data": [north.Owner, north.Employee]},
                { "name": "ภาคกลาง", "data": [centrals.Owner, centrals.Employee]},
                { "name": "ภาคตะวันออก", "data": [northerns.Owner, northerns.Employee]},
                { "name": "ภาคตะวันตก", "data": [east.Owner, east.Employee]},
                { "name": "ภาคใต้", "data": [south.Owner, south.Employee]}
            ]
        };

        var WorkOccupOptions = {
            lineSmooth: false,
            low: 0,
            high: 90,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var WorkOccupResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar("#Example15", WorkOccupData, WorkOccupOptions, WorkOccupResponsive);
    });

    // -------------------- Example Graph - Device vs. Career --------------------- //

    var Owners = {
        'Android': 0,
        'Iphone': 0,
        'Unknowns': 0
    };

    var Employees = {
        'Android': 0,
        'Iphone': 0,
        'Unknowns': 0
    };

    usersRef.on("value", function(res){
        res.forEach(function(user){

            var device = user.child('currentApp/profile/device/device').val();
            var Occupation = user.child('currentApp/occupation').val();

            if(Occupation == 'owner' && device == 'android'){
                Owners['Android'] += 1;
            }else if(Occupation == 'owner' && device == 'iphone') {
                Owners['Iphone'] += 1;
            }else if(Occupation == 'owner' && device == 'unknown'){
                Owners['Unknowns'] += 1;

            }else if(Occupation == 'employee' && device == 'android'){
                Employees['Android'] += 1;
            }else if(Occupation == 'employee' && device == 'iphone') {
                Employees['Iphone'] += 1;
            }else if(Occupation == 'employee' && device == 'unknown') {
                Employees['Unknowns'] += 1;
            }
        });

        console.log("● จำนวนผู้สมัครกู้ แยกตามเอาชีพและ device ที่ใช้ ");

        console.log('เจ้าของธุรกิจ',  Owners);
        console.log('พนักงานประจำ', Employees);


        var CareerAndDeviceData = {
            labels: ['Android', 'iPhone', 'Unknown'],
            series: [
                { "name": "เจ้าของธุรกิจ", "data": [Owners.Android,Owners.Iphone,Owners.Unknowns]},
                { "name": "พนักงานประจำ", "data": [Employees.Android, Employees.Iphone,Employees.Unknowns]}
            ]
        };

        var CareerAndDeviceOptions = {
            lineSmooth: false,
            low: 0,
            high: 150,
            height: "249px",
            area: true,
            axisX: {
                showGrid: true
            }
        };

        var CareerAndDeviceResponsive = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];

        Chartist.Bar("#Example16", CareerAndDeviceData, CareerAndDeviceOptions,CareerAndDeviceResponsive);
    });

    function Search(user) {
        var search = document.getElementById("mySearch");
        document.getElementById("search01").innerHTML = search;

                var User = user.child('currentApp/profile/firstname').val();

    }

});

